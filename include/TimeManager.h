
#ifndef TIMEMANAGER_H
#define TIMEMANAGER_H

// forward declarations
class StateManager;
class WorldManager;
class InputManager;

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class TimeManager
{
public:
	TimeManager()	{ clear(); }
	~TimeManager()	{ destroy(); }

	void clear();
	bool create( StateManager& stateManager, WorldManager& worldManager, InputManager& inputManager );
	void destroy();
	
	void runRenderLoop();

private:
	Root*			root_;
	StateManager*	stateManager_;
	WorldManager*	worldManager_;
	InputManager*	inputManager_;
	Timer*			timer_;
};

#endif // TIMEMANAGER_H