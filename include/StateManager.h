
#ifndef STATEMANAGER_H
#define STATEMANAGER_H

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

typedef enum
{
	RUNNING,
	SHUTDOWN
}	State;

class StateManager
{
public:
	StateManager()	{ clear(); }
	~StateManager()	{ destroy(); }

	void clear();
	bool create();
	void destroy();

	State	getCurrentState();
	bool	requestStateChange( State newState );

private:
	State	currentState_;
};

#endif // STATEMANAGER_H