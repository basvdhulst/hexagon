
#ifndef WORLDMANAGER_H
#define	WORLDMANAGER_H

// including headers
#include <Ogre.h>
#include "CameraManager.h"
#include "HexGrid.h"

// using namespaces
using namespace Ogre;

class WorldManager
{
public:
	WorldManager()	{ clear(); }
	~WorldManager()	{ destroy(); }

	void clear();
	bool create();
	void destroy();
	void update( float dtimeInSecs );

	CameraManager*	getCameraManager();

private:
	SceneManager*	sceneManager_;
	CameraManager	cameraManager_;
	HexGrid			hexGrid_;
};

#endif // WORLDMANAGER_H
