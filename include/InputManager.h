
#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

// forward declarations
class StateManager;
class WorldManager;

// including headers
#include <Ogre.h>
#include <OISInputManager.h>
#include <OISMouse.h>
#include <OISKeyboard.h>

// using namespaces
using namespace Ogre;

class InputManager : public OIS::MouseListener, OIS::KeyListener
{
public:
	InputManager()	{ clear(); }
	~InputManager()	{ destroy(); }

	void clear();
	bool create( StateManager& stateManager, WorldManager& worldManager );
	void destroy();
	void update();

private:
	RenderTarget*		renderTarget_;
	StateManager*		stateManager_;
	WorldManager*		worldManager_;
	OIS::InputManager*	oisInputManager_;
	OIS::Mouse*			mouse_;
	OIS::Keyboard*		keyboard_;

	// input events
	bool keyPressed		( const OIS::KeyEvent &evt );
	bool keyReleased	( const OIS::KeyEvent &evt );
	bool mouseMoved		( const OIS::MouseEvent &evt );
	bool mousePressed	( const OIS::MouseEvent &evt, OIS::MouseButtonID but );
	bool mouseReleased	( const OIS::MouseEvent &evt, OIS::MouseButtonID but );
};

#endif // INPUTMANAGER_H
