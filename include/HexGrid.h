
#ifndef HEXGRID_H
#define HEXGRID_H

// including headers
#include <Ogre.h>
#include "HexGridTile.h"

// using namespaces
using namespace Ogre;

class HexGrid
{
public:
	HexGrid()	{ clear(); }
	~HexGrid()	{ destroy(); }

	void clear();
	bool create( ushort width = 5, ushort height = 5, ushort tileSize = 20 );
	void destroy();
	void show();

private:
	ushort			width_;
	ushort			height_;
	ushort			tileSize_;
	SceneManager*	sceneManager_;
	HexGridTile*	tileMatrix_;

	void createHexEntity();
	void createTileMatrix();
};

#endif // HEXGRID_H
