
#ifndef APPLICATION_H
#define APPLICATION_H

// including headers
#include <Ogre.h>
#include "WorldManager.h"
#include "StateManager.h"
#include "InputManager.h"
#include "TimeManager.h"

// using namespaces
using namespace Ogre;

class Application
{
public:
	Application()	{ clear(); }
	~Application()	{ destroy(); }

	void clear();
	bool create();
	void destroy();
	void run();

private:
	Root*					root_;
	RenderSystem*			renderSystem_;
	
	// Factory classes
	WorldManager	worldManager_;
	StateManager	stateManager_;
	InputManager	inputManager_;
	TimeManager		timeManager_;
	
	void initializePlugins();
	void initializeRenderSystem();
	void initializeRenderWindow();
	void initializeResources();
};

#endif // APPLICATION_H
