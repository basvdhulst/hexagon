
#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class CameraManager
{
public:
	CameraManager()	{ clear(); }
	~CameraManager(){ destroy(); }

	void clear();
	bool create();	
	void destroy();
	void update( float dtimeInSecs );

	void rotateLeft();
	void rotateRight();
	void rotateStop();

private:
	SceneManager*	sceneManager_;
	RenderTarget*	renderTarget_;
	Camera*			camera_;
	Viewport*		viewport_;
	SceneNode*		cameraNode_;				// node to which the camera is attached at a distance
	float			radialVel_;					// radial velocity of camera rotation
	float			radialEndVel_;				// radial velocity end value of camera rotation
	float			radialAccInput_;
	
	void updateRotate( float dtimeInSecs );
};


#endif // CAMERAMANAGER_H