
#ifndef HEXGRIDTILE_H
#define HEXGRIDTILE_H

// including headers
#include <Ogre.h>

// using namespaces
using namespace Ogre;

class HexGridTile
{
public:
	HexGridTile()	{ clear(); }
	~HexGridTile()	{ destroy(); }

	void	clear();
	bool	create( const Vector3 &position, ushort tileSize = 20 );
	void	destroy();
	void	show();

private:
	SceneManager*	sceneManager_;
	ushort			tileSize_;
	SceneNode*		tileNode_;
	Entity*			tileEnt_;
};

#endif // HEXGRIDTILE_H