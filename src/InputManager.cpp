
// including headers
#include "InputManager.h"
#include "StateManager.h"
#include "WorldManager.h"



void InputManager::clear()
{
	renderTarget_		= 0;
	stateManager_		= 0;
	worldManager_		= 0;
	oisInputManager_	= 0;
	mouse_				= 0;
	keyboard_			= 0;
}



bool InputManager::create( StateManager& stateManager, WorldManager& worldManager )
{
	renderTarget_	= Root::getSingleton().getRenderTarget( "BaseSoftware" );
	stateManager_	= &stateManager;
	worldManager_	= &worldManager;

	OIS::ParamList list;
	std::ostringstream windowHndStr;
	uint64 hWnd = 0;
	renderTarget_->getCustomAttribute( "WINDOW", &hWnd );
	windowHndStr << hWnd;
	list.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
	oisInputManager_ = OIS::InputManager::createInputSystem(list);

	mouse_		= static_cast<OIS::Mouse*>(oisInputManager_->createInputObject(OIS::OISMouse, true));
	keyboard_	= static_cast<OIS::Keyboard*>(oisInputManager_->createInputObject(OIS::OISKeyboard, true));
	mouse_		->setEventCallback(this);
	keyboard_	->setEventCallback(this);

	return true;
}



void InputManager::destroy()
{
	if(mouse_)
		oisInputManager_->destroyInputObject(mouse_);
	
	if(keyboard_)
		oisInputManager_->destroyInputObject(keyboard_);
	
	OIS::InputManager::destroyInputSystem(oisInputManager_);
	clear();
}



bool InputManager::keyPressed( const OIS::KeyEvent &evt )
{
	if( evt.key == OIS::KC_ESCAPE )
		stateManager_->requestStateChange( SHUTDOWN );

	if( evt.key == OIS::KC_A )
		worldManager_->getCameraManager()->rotateLeft();

	if( evt.key == OIS::KC_D )
		worldManager_->getCameraManager()->rotateRight();

	return true;
}



bool InputManager::keyReleased( const OIS::KeyEvent &evt )
{
	if( evt.key == OIS::KC_A || evt.key == OIS::KC_D )
		worldManager_->getCameraManager()->rotateStop();

	return true;
}



bool InputManager::mouseMoved( const OIS::MouseEvent &evt )
{
	return true;
}



bool InputManager::mousePressed( const OIS::MouseEvent &evt, OIS::MouseButtonID but )
{
	return true;
}



bool InputManager::mouseReleased( const OIS::MouseEvent &evt, OIS::MouseButtonID but )
{
	return true;
}



void InputManager::update() 
{
	mouse_		->capture();
	keyboard_	->capture();
}