
// including headers
#include "StateManager.h"
#include "InputManager.h"



void StateManager::clear()
{
}



bool StateManager::create()
{
	return true;
}



void StateManager::destroy()
{
	clear();
}



State StateManager::getCurrentState()
{
	return currentState_;
}



bool StateManager::requestStateChange( State newState ) 
{
	if(newState == currentState_) {
		return false;
	}

	currentState_ = newState;
	return true;
}