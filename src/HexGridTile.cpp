
// including headers
#include "HexGridTile.h"



void HexGridTile::clear()
{
	sceneManager_	= 0;
	tileNode_		= 0;
	tileEnt_		= 0;
}



bool HexGridTile::create( const Vector3 &position, ushort tileSize )
{
	sceneManager_	= Root::getSingleton().getSceneManager( "sceneManager" );
	tileNode_		= sceneManager_->getRootSceneNode()->createChildSceneNode( position );
	tileEnt_		= sceneManager_->createEntity( "tileMesh" );
	tileSize_		= tileSize;

	return true;
}



void HexGridTile::destroy()
{
	clear();
}



void HexGridTile::show()
{
	tileNode_->attachObject( tileEnt_ );
}