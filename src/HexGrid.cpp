
// including headers
#include "HexGrid.h"



void HexGrid::clear()
{
	sceneManager_	= 0;
	tileMatrix_		= 0;
}



bool HexGrid::create( ushort width, ushort height, ushort tileSize )
{
	width_			= width;
	height_			= height;
	tileSize_		= tileSize;
	sceneManager_	= Root::getSingleton().getSceneManager( "sceneManager" );
	tileMatrix_		= new HexGridTile[width_*height_];
	
	createHexEntity();
	createTileMatrix();

	return true;
}



void HexGrid::createHexEntity()
{
	ManualObject* tileObj = sceneManager_->createManualObject();
	tileObj->begin( "tileMat", RenderOperation::OT_LINE_STRIP );
	tileObj->position( -0.25f*tileSize_, 0, -0.5f*tileSize_ );
	tileObj->position( 0.25f*tileSize_, 0, -0.5f*tileSize_ );
	tileObj->position( 0.5f*tileSize_, 0, 0 );
	tileObj->position( 0.25f*tileSize_, 0, 0.5f*tileSize_ );
	tileObj->position( -0.25f*tileSize_, 0, 0.5f*tileSize_ );
	tileObj->position( -0.5f*tileSize_, 0, 0 );
	tileObj->position( -0.25f*tileSize_, 0, -0.5f*tileSize_ );
	tileObj->end();
	tileObj->convertToMesh( "tileMesh" );
}



void HexGrid::createTileMatrix()
{
	for( ushort j = 0; j < height_; ++j )
	{
		for( ushort i = 0; i < width_; ++i )
		{
			float posx = static_cast<float>( (i+1)*0.75*tileSize_ );
			float posy = static_cast<float>( (j+1)*tileSize_+(i&1)*0.5*tileSize_ );
			tileMatrix_[i+j*width_].create( Vector3( posx, 0, posy ) );
		}
	}
}



void HexGrid::destroy()
{
	delete[] tileMatrix_;
	clear();
}



void HexGrid::show()
{
	for( ushort j = 0; j < height_; ++j )
	{
		for( ushort i = 0; i < width_; ++i )
			tileMatrix_[i+j*width_].show();
	}
}