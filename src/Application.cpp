
// including headers
#include "Application.h"



void Application::clear()
{
	root_			= 0;
	renderSystem_	= 0;
}




bool Application::create()
{
	root_ = new Root("","");

	initializePlugins();
	initializeRenderSystem();
	initializeRenderWindow();
	initializeResources();

	stateManager_	.create();
	worldManager_	.create();
	inputManager_	.create( stateManager_, worldManager_ );
	timeManager_	.create( stateManager_, worldManager_, inputManager_ );

	return true;
}



void Application::destroy()
{
	delete root_;
	clear();
}



void Application::run()
{
	timeManager_.runRenderLoop();
}



void Application::initializePlugins()
{
	ConfigFile cf;
	
	#ifdef _DEBUG
		cf.load( "plugins_d.ini" );
	#else
		cf.load( "plugins.ini" );
	#endif

	ConfigFile::SectionIterator seci = cf.getSectionIterator();
	ConfigFile::SettingsMultiMap *set = seci.getNext();
	ConfigFile::SettingsMultiMap::iterator i;
	
	for (i = set->begin(); i != set->end(); ++i)
		root_->loadPlugin( i->second );
}



void Application::initializeRenderSystem() 
{
	RenderSystemList list = root_->getAvailableRenderers();
	RenderSystemList::iterator i = list.begin();

	if(i != list.end() && !renderSystem_) {
		renderSystem_ = *i;
		root_->setRenderSystem( renderSystem_ );
	}

	if(root_->getRenderSystem() == 0)
		throw Exception( 1, "Error: No render system available", "" );
	else
		root_->initialise( false );
}



void Application::initializeRenderWindow()
{
	ConfigFile cf;
	cf.load( "wndconf.ini" );
	ConfigFile::SectionIterator i = cf.getSectionIterator();
	ConfigFile::SettingsMultiMap* map = i.getNext();
	ConfigFile::SettingsMultiMap::iterator j;

	ushort	video_width	 = 1280;
	ushort	video_height = 1024;
	bool	fullscreen	 = true; 

	NameValuePairList list;
	String parameter,value;

	for (j = map->begin(); j != map->end(); ++j) {
		parameter = j->first;
		value = j->second;

		if(parameter.compare( "width" ) == 0)
			video_width = StringConverter::parseUnsignedInt( value );
		else if(parameter.compare( "height" ) == 0)
			video_height = StringConverter::parseUnsignedInt( value );
		else if(parameter.compare( "fullscreen" ) == 0)
			fullscreen = (value.compare( "true" ) == 0) ? true : false;
		else
			list[j->first] = j->second;
	}

	Ogre::RenderWindow *window = root_->createRenderWindow( "BaseSoftware", video_width,
															video_height, fullscreen, &list );
	window->setActive(true);
}



void Application::initializeResources()													//In the end must be replaced by a single .zip recource load?
{
	
	ConfigFile cf;
	cf.load( "resources.ini" );
	ConfigFile::SectionIterator i = cf.getSectionIterator();

	String secName;
	while (i.hasMoreElements())
	{
		secName = i.peekNextKey();
		ConfigFile::SettingsMultiMap* map = i.getNext();
		ConfigFile::SettingsMultiMap::iterator j;
		for (j = map->begin(); j != map->end(); ++j)
			ResourceGroupManager::getSingleton().addResourceLocation(j->second, j->first, secName);
	}

	ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}
