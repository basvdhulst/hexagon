
// including headers
#include "TimeManager.h"
#include "StateManager.h"
#include "WorldManager.h"
#include "InputManager.h"
#include <OgreWindowEventUtilities.h>


void TimeManager::clear()
{
	root_			= 0;
	stateManager_	= 0;
	worldManager_	= 0;
	inputManager_	= 0;
	timer_			= 0;
}



bool TimeManager::create( StateManager& stateManager, WorldManager& worldManager, InputManager& inputManager )
{
	root_			= Root::getSingletonPtr();
	stateManager_	= &stateManager;
	worldManager_	= &worldManager;
	inputManager_	= &inputManager;
	timer_			= root_->getTimer();

	return true;
}



void TimeManager::destroy()
{
	clear();
}



void TimeManager::runRenderLoop()
{
	uint64 timeOld		= timer_->getMicroseconds();
	float dtimeInSecs	= 0.017f;								// 0.017 corresponds to FPS ~ 59
	uint64 timeNew;

	while(stateManager_->getCurrentState() != SHUTDOWN)
	{
		inputManager_	->update();
		worldManager_	->update( dtimeInSecs );
		root_			->renderOneFrame();
		timeNew			= timer_->getMicroseconds();
		dtimeInSecs		= (timeNew - timeOld)/1000000.0f;
		timeOld			= timeNew;
	}
}
