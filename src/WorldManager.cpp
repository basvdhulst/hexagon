
// including headers
#include "WorldManager.h"



void WorldManager::clear()
{
	sceneManager_	= 0;
}



bool WorldManager::create()
{
	sceneManager_	= Root::getSingleton().createSceneManager( ST_GENERIC, "sceneManager" );
	cameraManager_	.create();
	hexGrid_		.create( 15, 15 );
	hexGrid_		.show();

	return true;
}



void WorldManager::destroy()
{
	clear();
}



CameraManager* WorldManager::getCameraManager()
{
	return &cameraManager_;
}



void WorldManager::update( float dtimeInSecs )
{
	cameraManager_.update( dtimeInSecs );
}