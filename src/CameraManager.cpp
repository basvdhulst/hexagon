
// including headers
#include "CameraManager.h"



void CameraManager::clear()
{
	camera_			= 0;
	viewport_		= 0;
	radialVel_		= 0;
	radialEndVel_	= 0;
	radialAccInput_	= 0;
}



bool CameraManager::create()
{
	sceneManager_	= Root::getSingleton().getSceneManager( "sceneManager" );
	renderTarget_	= Root::getSingleton().getRenderTarget( "BaseSoftware" );

	camera_		= sceneManager_->createCamera( "camera" );
	cameraNode_	= sceneManager_->getRootSceneNode()->createChildSceneNode( Vector3( 100, 0, 100 ) );
	cameraNode_	->attachObject( camera_ );
	camera_		->setPosition( 200, 200, 200 );
	camera_		->lookAt( cameraNode_->getPosition() );
	viewport_	= renderTarget_->addViewport( camera_ );

	return true;
}



void CameraManager::destroy()
{
	clear();
}



void CameraManager::rotateLeft()
{
	radialAccInput_ = -7.0f;
}



void CameraManager::rotateRight()
{
	radialAccInput_ = 7.0f;
}



void CameraManager::rotateStop()
{
	radialAccInput_ = 0.0f;
}



void CameraManager::update( float dtimeInSecs )
{
	updateRotate( dtimeInSecs );
}



void CameraManager::updateRotate( float dtimeInSecs )
{
	float radialAcc_	= radialAccInput_ - radialVel_*7.0f;
	radialVel_			+= radialAcc_ * dtimeInSecs;
	
	cameraNode_->rotate( Vector3::UNIT_Y, Radian( radialVel_*dtimeInSecs ), Node::TS_WORLD );
}