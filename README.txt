************************
Hexagon (2011)
************************

Automatic generation of a hexagonal world.

Libraries: Ogre3D, OIS

NOTE: Project was made in Windows with older version of Ogre. At this 
time I was unsuccessful in converting the files to Linux and Ogre v1-9 
(black screen).
