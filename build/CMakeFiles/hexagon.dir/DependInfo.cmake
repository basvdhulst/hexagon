# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/Application.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/Application.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/CameraManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/CameraManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/HexGrid.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/HexGrid.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/HexGridTile.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/HexGridTile.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/InputManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/InputManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/StateManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/StateManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/TimeManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/TimeManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/WorldManager.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/WorldManager.cpp.o"
  "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/src/main.cpp" "/home/bas/Documents/Programmeren/Projects/C++/Hexagon/build/CMakeFiles/hexagon.dir/src/main.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../include"
  "/usr/include/ois"
  "/usr/local/include/OGRE"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
